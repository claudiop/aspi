\documentclass[conference]{IEEEtran}
\IEEEoverridecommandlockouts

\usepackage[utf8]{inputenc}
\usepackage{cite}
\usepackage{graphicx}
\usepackage{textcomp}
\usepackage{hyperref}
\def\BibTeX{{\rm B\kern-.05em{\sc i\kern-.025em b}\kern-.08em
    T\kern-.1667em\lower.7ex\hbox{E}\kern-.125emX}}

\begin{document}
\makeatletter
\newcommand{\newlineauthors}{%
  \end{@IEEEauthorhalign}\hfill\mbox{}\par
  \mbox{}\hfill\begin{@IEEEauthorhalign}
}
\makeatother

\title{Ethical Hacking\\
{\small Aspetos Socio-Profissionais de Informática}}

\author{
	\IEEEauthorblockN{Ana Sofia Afonso}
	\IEEEauthorblockA{
		\textit{48303} \\
		\textit{FCT NOVA}\\
		asl.afonso@campus.fct.unl.pt}
\and
	\IEEEauthorblockN{Cláudio Afonso de Sousa Pereira}
	\IEEEauthorblockA{
		\textit{51717} \\
		\textit{FCT NOVA}\\
		cad.pereira@campus.fct.unl.pt}
\and
	\IEEEauthorblockN{Cláudio Nuno Rodrigues Pereira}
	\IEEEauthorblockA{\textit{47942} \\
	\textit{FCT NOVA}\\
	cn.pereira@campus.fct.unl.pt}
\newlineauthors
	\IEEEauthorblockN{Gabriel Lopes Batista}
	\IEEEauthorblockA{
		\textit{47590} \\
		\textit{FCT NOVA}\\
		g.baptista@campus.fct.unl.pt}
\and
	\IEEEauthorblockN{Luis Pedro Lança}
	\IEEEauthorblockA{
		\textit{47205} \\
		\textit{FCT NOVA}\\
		l.lanca@campus.fct.unl.pt}
}
\maketitle

\begin{abstract}
This paper revolves around ethical hackers as and the implications of their work on society, considering both ethical hacking as a job as well as moral hacking.
\end{abstract}

\begin{IEEEkeywords}
	Ethical hackers, Security, Professional Certifications, Security fields,
	Laws, Moral, Movements, Society, Ethics
\end{IEEEkeywords}

\section{Introduction}
Throughout history, as computers have become more widely available a culture has grown with them; a culture of people who saw computers as a very flexible tools and used them despite their jobs, producing new ideas and advancing knowledge with their inventions. The members of that community begun referring to themselves as ``\textit{hackers}".\cite{charlespalmer}

Through the past decades there have been multiple definitions of what hacking is.\cite{HackingGenealogy}
An attempt to verbalize the root idea of the movement is:
\begin{quote}
``Hacking means exploring the limits of what is possible, in a spirit of playful cleverness".\cite{StallmanFreeSociety}
\end{quote}
Since this definition is broad, it often employed on multiple occasions, even outside of computer science groups to refer to quotidian ingenuity.\cite{RosnerIKEA}\cite{HackingGenealogy}

One of the other meanings the word gained trough time was the act of maliciously bypassing computer security, and shortly after, confusingly enough, its antonym, the act of protecting computer security.\cite{HackingGenealogy} Through this article we will often use the latter as our meaning of hacking, a technician that works in the field of computer security.

Since computers were costly at the time, some of their users had restricted access. With the desire of bypassing those restrictions some users explored the system bugs and other vulnerabilities, often with benign interests in mind (such as having more resources allocated to a task).\cite{charlespalmer}
When computer intrusions became more noticeable and less benign, the media begun referring to the term ``\textit{hacker}" pejoratively, as intruders and thieves.\cite{charlespalmer} \cite{TeachingStudentsToHackUniversityLevel}
Thus the security context, an act of \textit{hacking} can be defined as ``accessing a system that one is either not authorized to access or who accesses a system at a level beyond their authorization". \cite{TeachingStudentsToHackUniversityLevel}

In recent years, two new terms appeared, ``\textit{ethical hacking}" and ``\textit{hacktivism}".
Their meanings intersect, with ethical hacking referring to benign \textit{hacking} of any nature, and \textit{hacktivism} as \textit{hacking} with (often benign) social causes in mind.\cite{ColemanEthicsOfHacking}\cite{GunkelHackingHacktivism}

Some scenarios that fall under ethical hacking:
\begin{itemize}
	\item Benign security testing.
	\item The unpurposeful usage of computers for social causes.
	\begin{itemize}
		\item The right to repair.
		\item Propagation of information.
		\item Organization of social movements.
	\end{itemize}
\end{itemize}

Though this paper we'll cover those scenarios, with the first part talking about security ethical hackers, and the second about ethical hacktivists

\section{Shades of gray}
It is common for hackers to be compared to shades of gray, that represent the morality of their hacks. The commonly employed terms are ``white hat", ``gray hat" and ``black hat".

White hat hackers are those who are considered to have good moral values, always being ethical hackers, thus are desired by companies for their security jobs.

Black hats can also be called ``crackers", and are considered unethical. They use their knowledge and abilities to their own advantage above everything else. Their actions often include the access of information without authorization and modifying or deleting information when convenient. \cite{TeachingStudentsToHackUniversityLevel}\cite{EthicalHackingPenetrationTesting}

Gray hats are considered to be in between white hats and black hats.
Some ethical doctrines defend that apparently unethical actions can be ethical if ``the end justifies the means", and that's a common position for a gray hat to assume.
They are usually ethical, but might take unethical actions when they consider them as as needed or lesser evils.

\section{Security hacking}
A possible scenario of ethical hacking is security hacking (also known as penetration testing) which is defined as attacking a system on behalf of its owners, seeking vulnerabilities that a malicious hacker can exploit.
Ideally, hired hackers find vulnerabilities in the organization systems before real attackers do and, letting the organization coders know and correct them.

Hired hackers have to be trustworthy, and act wisely to avoid damaging the company systems and information, thus they are usually experts in risk management as they are in their fields of work. \cite{EthicalHackingRiskManagement} \cite{EthicalHackingPenetrationTesting}

The most trustworthy hackers in a company, that are believed to be white hats, often have explicit permission to access and break into the organization's systems and use legal procedures as part of their work. \cite{TeachingStudentsToHackUniversityLevel} \cite{EthicalHackingPenetrationTesting} \cite{CybersecurityEthicalHackingSME}

Gray hat hackers might be desirable, and they may attempt to attack a system without permission, but on success they inform the organization that it was done with good intentions and provide details on the weakness. \cite{CybersecurityEthicalHackingSME}
Gray hat hackers often do justice with the own hands, without being backed by the law, for example to protect employees from an abusive boss, and as such might present a risky preposition for an organization. (cite this)

\subsection{Professional Certifications}
The original hackers movement, that happened around the 1960's, was mostly made from white hat hackers.
Since they were ethical it was common for security to be neglected, they did not want to cause harm others until a few years later. Once the first grey and black hat hackers appeared the media outlets started to portray the term ``hacker" associated with criminal activities. \cite{HackingCertification1}

Because of this association, organizations needed a way to distinguish the ethical, white hat hackers from the malicious black hats. This need was felt specially when companies started moving their services to the web and required more information security experts than ever before.

This need was satisfied with the appearance of information security certifications.
Starting in the 1990s some organizations began certifying security professionals as ethical, one of the most popular certifications being the Certified Ethical Hacker (CEH).
There are many others, but a common feature present in such certifications is a code of ethics that applicants must sign and swear to follow.

These certifications were well received by organizations as they saw it as an extra assurance of their workers loyalty. It was also a way to help the training centers stand out from the nefarious side, that hacking was known for. \cite{HackingCertification2}

But professionals with those certifications do not always follow the code that they agreed on.
Edward Snowden, a famous whistleblower who leaked highly classified information gathered by the National Security Agency(N.S.A.) was a certified ethical hacker and did sign the corresponding code of ethics many allege that he broke it during the events at N.S.A. \cite{HackingCertification3}

Codes of conduct often introduce contradictions. Some authors state that Snowden broke the code by exposing the information that he sweared to protect. Some allege that Snowden would break that very same code by not leaking the government violations of the law. \cite{DelmasGovernmentWhistle}

Many hackers do not really value these certificates and express that it is mostly used by organizations to choose between job candidates when there are no other factors to compare them. Some also state that most of what is taught at these training centers is very broad and basic and does not reflect real life situations.

These certificates were mostly created to tap into the growing market of information security. \cite{HackingCertification2}

\subsection{Case Study - Climate Change Data Rescue}
On January 20th of 2017, the day US president Donald Trump was sworn into power, in the University of California, Los Angeles and in the University of Pennsylvania, programmers and scientists were hacking government websites to harvest data. Among their main targets were documents about the Department of Energy's solar power initiative, hydrogen fuel research and studies comparing the efficiency of fossil fuels to renewable energy resources.\cite{USClimateDataRescue1}
\cite{USClimateDataRescue2}

They started the data rescuing hack marathon weeks before Trump took office, and managed to save hundreds of thousands of pages off 4 major US government websites: The Environmental Protection Agency, NASA, The White House and The Department of Energy.

These activists were not only motivated by Donald Trump's views on climate change, but also the destruction of environmental data performed by Canadian Prime Minister Stephen Harper. As they suspected would happen, exactly at noon in the day Trump took office, all pages related to climate change were deleted off the White House's website. 

Some of the data being rescued was easily accessible by using the websites as they were intended, but some data-sets, like the Environmental Protection Agency's map of greenhouse emissions, had to be accessed through illegal means, namely, the use of a backdoor. A backdoor is a way of accessing a computer system without going through the intended authentication, although it can also be used with authentication that was illicitly acquired.\cite{BackDoor1}

The rescued data was uploaded to the DataRefuge.org repository, located in the US and Page Freezer in Europe, in an attempt to keep the data safe by locking it out of US legislation.\cite{USClimateDataRescue3}

This data rescue hack is a clear example of how hacking can be used ethically, and moreover, of how not all illegal acts are immoral. 

\section{Ethical defenses in a connected world}
It can be contested if an organization is able defend itself employing only white hat hackers, that are completely ethical. Due to the fact that white hats often have strict limits on what attacks they can preform and are dependent on authorizations and the respective bureaucracy they can become conditioned.\

However there are some solutions that help white hackers provide an effective defense to some corporations:
\subsection{Honeypots - The ethical aggressive defense}
One well known defense mechanism is called an \textit{honeypot}, a technique which employs a decoy that mimics the service with the purpose of monitoring the received attacks.\cite{Honeypots1}\cite{ScottbergHoneypotsEntrapment}
Honeypots are isolated from the real service, thus presenting no harm to the company. They do:
\begin{itemize}
    \item Distract attackers, giving them a fake target instead of the real, valuable service;
    \item Expose and reveal attacks (and the respective exploitation strategies);
    \item Gather information about the attacker (personal information and desired information);
\end{itemize}

Honeypots can be both ``research honeypots", designed solely to gain information about the black hat community, and ``production honeypots", which mirror the production network of the company, inviting attackers to interact with them to expose the current vulnerabilities of the system.

Their use can however present ethical and legal challenges:
\subsubsection{Entrapment}
Defined as:
\begin{quote}
	``[...] the conception and planning of an offense by an officer, and his procurement of its commission by one who would not have perpetrated it except for the trickery, persuasion, or fraud of the officer."\cite{SorrellsUnitedStates}
\end{quote}
Is a potential risk of using honeypots, prompting attacks that would not happen otherwise, as well as voiding potential lawsuits if proven.

\subsubsection{Privacy}
An outlaw attack does not grant the defensor the right to use outlaw means to combat.
Not everything can be monitored, the attacker has a right to privacy unless revoked by a judge, and so do the other users, thus the honeypot can only monitor within a reasonable scope, considered what was consented by the service users. \cite{ScottbergHoneypotsEntrapment}

\subsection*{Legal issues}
Albeit ethics are usually not based on the law, the opposite is often true. It is not uncommon for the association between lawlessness and unethicality to be true.
While ethics have to evaluated on a case by case basis, the current legislation on this regard in Portugal is:
\begin{itemize}
    \item Article 12, United Nations declaration of Human rights;
    \item Article 8, European convention on Human Rights;
    \item Article 35, Constitution of the Portuguese Republic;
    \item Data protection Act, Law nº 67/98 (October 26);
    \item General Data Protection Regulation (EU 2016/67);
\end{itemize}

The contents of those laws are mostly outside the scope of ethical hacking, however they are very closely related to ethics.

\section{Social hacking}
Hackers play a very important role in society, both ethical white hat hackers and arguably gray hat hats empower society, by giving a voice to the people, increasing transparency and promoting education.

\subsection{Computer science liberalization}
The internet was originally meant to be used by the military (1960's), mostly as communication backup in case of a nuclear war. The academia, which had most of the hackers at the time, tinkered with the \textit{ARPANET} and the ensuing networks at the time to create what begun to be known as the modern day internet.

In the 1980's, the internet was an unregulated, made of several institutions and communities and worked on the basis of trust. Everything from email servers to the very hardware that lifted the internet was built on the basis that no user was malicious. Encryption was rare and validations almost non-existent.

With the influx of new users and the development of the for-profit internet, the network value raised, and the black hats begun to appear.

Some ethical hackers begun developing systems to keep the network as open as possible, with new protocols and softwares. A crucial step was the 1983's GNU project, leaded by Richard Stallman. The project intent was to stop knowledge from being hidden away as computer systems begun to be made more obscure, and to develop open and trustworthy alternatives to the existing software products.

The GNU project, which still exists today is one of the main pieces of the hacktivist movement, that leaded to the growth of a concept that is common nowadays:
\subsubsection*{Open source}
An open source product (usually software) is a product in which the build process can be freely audited and changed by anyone. The logic is not hidden from the user who is able to understand and modify the product.

Open source is very popular among hackers.
Security hackers are some of the most benefited from open source products, as they often get their tooling from the internet and modify them to their likes.
Also, open source software tend to avoid security from obscurity, the security based on the belief that an attacker does not know how to attack a given software.

With open source, crucial projects such as OpenSSL are constantly audited by every kind of hacker, and while sometimes black hat hackers find zero-days \footnote{Bugs that were unknown and are exploitable to weaken the security} it is a rare occurrence and there are huge benefits for ethical hackers who can rely on common tools that are known to be trustworthy.

Many hacktivists promote open source software as an ethical solution for many society problems, one of them being the right to repair.
\subsection{The right to repair}
Some software and hardware producers ship products that they claim as unrepairable by third parties.
Their terms strictly forbid users from attempting to fix problems, giving exclusive repair rights to the brand, which often charges artificially high values to incentive purchasing new products in detriment of repairing.

The right to repair was born from the aversion to such policies, which not only are unethical but promote waste. Many people are competent enough to fix their own products, and even when they are not, many would prefer to be able to hire an ethical hacker that can do so.

There are hacker movements attempting to enforce the right to repair, but they are struggling to get past the industry lobby. One of them is \url{https://repair.org/}

Within the European Union ... (what is the law?)

\subsubsection{Case Study - Jon Deere tractors}
John Deere is the world's largest agricultural machinery maker. With the advent of cheap computers John Deere's tractors begun having electronic and software components, and deployed artificial restrictions to prevent farmers or hired hackers from fixing solve software or electronic problems.

With the laws that currently apply in some parts of the world, farmers that attempt to hack their own tractors (or hire someone to do so for them) even if act ethically are considered ``pirates", in violation of the copyright laws such as the DMCA, for breaking into a protected system.

Despite the tractor purchase, the farmers only own the hardware, with the software being locked away, a practice viewed by many as unethical.
\bibliographystyle{unsrt}
\bibliography{bibliography}
\end{document}

